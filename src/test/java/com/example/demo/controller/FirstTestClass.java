package com.example.demo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class FirstTestClass {


    @Test
    public void firstTest() {
        String input = "input String";
        assertEquals("input String", input);
    }

}
