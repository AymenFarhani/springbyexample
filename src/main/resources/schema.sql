CREATE TABLE STUDENT (
  ID bigint not null,
  FIRST_NAME varchar(255) not null,
  LAST_NAME varchar(255) not null,
  EMAIL varchar(255) not null,
  BIRTH_DATE DATE not null,
  primary key (id)
);

CREATE TABLE COURSES (
   STUDENT_ID bigint not null,
   COURSE varchar(255) not null
);