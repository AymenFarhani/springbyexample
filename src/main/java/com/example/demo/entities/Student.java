package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Student extends Person{

    @ElementCollection
    @CollectionTable(name = "courses", joinColumns = @JoinColumn(name = "student_id"))
    private List<String> course;

    public Student() {
        super();
    }

    public Student(Long id, String firstName, String lastName, String email, LocalDate birthDate,List<String> course) {
        super(id, firstName, lastName, email, birthDate);
        this.course = course;
    }

    public List<String> getCourse() {
        return course;
    }

    public void setCourse(List<String> course) {
        this.course = course;
    }
}
