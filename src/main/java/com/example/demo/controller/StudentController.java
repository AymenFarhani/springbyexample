package com.example.demo.controller;


import com.example.demo.entities.Student;
import com.example.demo.repositories.StudentRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class StudentController {

    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    @PostMapping("/students/{id}")
    public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
        Student savedStudent = studentRepository.save(student);
        return new ResponseEntity<>(savedStudent, HttpStatus.OK);
    }

    @GetMapping("/students")
    public ResponseEntity<List<Student>> getAllStudents() {
        return new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable(name = "id") Long id) {
        return new ResponseEntity<>(studentRepository.getOne(id), HttpStatus.OK);
    }

    @GetMapping("/studentsByDate")
    public ResponseEntity<List<Student>> findByBirthDateBetween(@RequestParam(name = "startDate") @DateTimeFormat(pattern ="yyyy-MM-dd") LocalDate startDate, @RequestParam(name = "endDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
        if(endDate == null){
            endDate = LocalDate.now();
        }
        return new ResponseEntity<>(studentRepository.findByBirthDateBetween(startDate, endDate), HttpStatus.OK);
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<Student> deleteStudentById(@PathVariable(name = "id") Long id) {
        studentRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
