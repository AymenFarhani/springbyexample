package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;


public class CICDPipelineController {

    @GetMapping("/pipeline")
    public ResponseEntity<String> getFirstTest(){
        return new ResponseEntity<>("First Pipeline", HttpStatus.OK);
    }
}
